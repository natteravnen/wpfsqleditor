﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFSQLEditor
{
    public class WPFViewModel: INotifyPropertyChanged
    {

        #region Validation

        private string _ValidationBackGroundColor = "Blue";
        public string ValidationBackGroundColor { get { return _ValidationBackGroundColor; } set { _ValidationBackGroundColor = value; NotifyPropertyChanged("ValidationBackGroundColor"); } }

        private string _ValidationForeGroundColor = "Blue";
        public string ValidationForeGroundColor { get { return _ValidationForeGroundColor; } set { _ValidationForeGroundColor = value; NotifyPropertyChanged("ValidationForeGroundColor"); } }

        #endregion Validation

        #region ReadOnly

        private string _ReadOnlyBackGroundColor = string.Empty;
        public string ReadOnlyBackGroundColor { get { return _ReadOnlyBackGroundColor; } set { _ReadOnlyBackGroundColor = value; NotifyPropertyChanged("ReadOnlyBackGroundColor"); } }

        private string _ReadOnlyForeGroundColor = string.Empty;
        public string ReadOnlyForeGroundColor { get { return _ReadOnlyForeGroundColor; } set { _ReadOnlyForeGroundColor = value; NotifyPropertyChanged("ReadOnlyForeGroundColor"); } }

        #endregion ReadOnly

        #region Properties

        private string _Status = "Starting";

        public string Status { get { return _Status; } set { _Status = value; NotifyPropertyChanged("Status"); } }

        private string _IntroLegend = string.Empty;

        public string IntroLegend { get { return _IntroLegend; }
            set
            {
                _IntroLegend = value;
                if (_IntroLegend.Contains("\\r\\n"))
                {
                    string newLine = System.Environment.NewLine;
                    _IntroLegend.Replace("\\r\\n", @newLine);
                }
                NotifyPropertyChanged("IntroLegend"); }
        }

        private string _MainTitle = "DNB SQL Editor";

        public string MainTitle { get { return _MainTitle; } set { _MainTitle = value; NotifyPropertyChanged("MainTitle"); } }

        #endregion Properties

                #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        #endregion INotifyPropertyChanged

    }
}
