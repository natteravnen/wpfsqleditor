﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace WPFSQLEditor
{
    public class Configuration
    {
        internal string SysLogIP { get; set; }
        internal string SysLogName { get; set; }
        internal int DebugLevel { get; set; }
        internal bool SysLogEnabled { get; set; }

        public Configuration()
        {
            try
            {
                //string location = Path.Combine(Application.StartupPath, "DNB_Extensions", "Kiwi");
                UriBuilder uri = new UriBuilder(Application.StartupPath);
                string path = Uri.UnescapeDataString(uri.Path);
                XmlDocument configurationDoc = new XmlDocument();
                configurationDoc.Load(Path.Combine(path, "Configuration.xml"));
                SysLogIP = configurationDoc.SelectSingleNode("/Configuration/SysLog/IP").InnerXml;
                SysLogEnabled = Convert.ToBoolean(configurationDoc.SelectSingleNode("/Configuration/SysLog/Enabled").InnerXml);
                SysLogName = configurationDoc.SelectSingleNode("/Configuration/SysLog/Name").InnerXml;
                DebugLevel = Convert.ToInt16(configurationDoc.SelectSingleNode("/Configuration/SysLog/DebugLevel").InnerXml);

            }
            catch (Exception ex)
            {
                MessageBox.Show("Fejl ved indlæsning af Syslog configuration: " + ex.Message);
            }
        }
    }
}
