﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace WPFSQLEditor
{
    public class DB_Data:INotifyPropertyChanged
    {
        string _ConString = string.Empty;
        string _CmdString = string.Empty;
        string _TableName = string.Empty;

        List<string> ReadOnlyFieldsList;
        List<string> HiddenFieldsList;
        List<string> ValidationFieldsList;

        DataTable orgData;
        SqlDataAdapter sda;
        SysLog myLog = new SysLog();

        public DB_Data()
        {
            ReadSetup();
            InitSyslog();
        }

        private void InitSyslog()
        {
            string _SysLogIP = ConfigurationManager.AppSettings["SyslogIP"];
            string _SyslogName = ConfigurationManager.AppSettings["SyslogName"];
            bool _SyslogEnabled = Convert.ToBoolean(ConfigurationManager.AppSettings["SyslogEnabled"]);
            int _DebugLevel = Convert.ToInt16(ConfigurationManager.AppSettings["DebugLevel"]);
            
            myLog.EnableLog(_SysLogIP, _SyslogName, _SyslogEnabled, _DebugLevel);
        }

        string ValidationBackGroundColor = ConfigurationManager.AppSettings["ValidationBackGroundColor"];
        string ValidationForeGroundColor = ConfigurationManager.AppSettings["ValidationForeGroundColor"];
        string ReadOnlyBackGroundColor = ConfigurationManager.AppSettings["ReadOnlyBackGroundColor"];
        string ReadOnlyForeGroundColor = ConfigurationManager.AppSettings["ReadOnlyForeGroundColor"];
        UniversalValueConverter stringToColor = new UniversalValueConverter();
        string _SearchFilterColumn = string.Empty;
        string _PrimaryKey = string.Empty;

        Dictionary<string, string> defaultValues = new Dictionary<string, string>();
        private void ReadSetup()
        {
            _ConString = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            HiddenFieldsList = new List<string>(ConfigurationManager.AppSettings["HiddenFields"].Split(new char[] { ';' }));
            ReadOnlyFieldsList = new List<string>(ConfigurationManager.AppSettings["ReadOnlyFields"].Split(new char[] { ';' }));
            ValidationFieldsList = new List<string>(ConfigurationManager.AppSettings["ValidationFields"].Split(new char[] { ';' }));
            _TableName = ConfigurationManager.AppSettings["TableName"];
            //_CmdString = string.Format("SELECT * FROM {0}", _TableName);
            _CmdString = ConfigurationManager.AppSettings["SqlStatement"];
            _SearchFilterColumn = ConfigurationManager.AppSettings["SearchFilterColumn"];
            _PrimaryKey = ConfigurationManager.AppSettings["PrimaryKey"];
           
            ValidationBackGroundColor = ConfigurationManager.AppSettings["ValidationBackGroundColor"];
            ValidationForeGroundColor = ConfigurationManager.AppSettings["ValidationForeGroundColor"];
            ReadOnlyBackGroundColor = ConfigurationManager.AppSettings["ReadOnlyBackGroundColor"];
            ReadOnlyForeGroundColor = ConfigurationManager.AppSettings["ReadOnlyForeGroundColor"];

            // Default Values KeyValueInternalCollection
            //NameValueConfigurationCollection defValues = (NameValueConfigurationCollection)ConfigurationManager.GetSection("DefaultValues");
            var defValues = ConfigurationManager.GetSection("DefaultValues") as NameValueCollection;
            if (defValues != null)
            {
                foreach (var i in defValues.AllKeys)
                {
                    string readVal = defValues.GetValues(i).FirstOrDefault();
                    defaultValues.Add(i, readVal);
                }
                //string[] allValues = defaultValues.AllKeys;
            }
        }

        public string CmdString { get { return _CmdString; } set { _CmdString = value; } }

        public string CurrentCommand { get; set; }

        public void Search(DataGrid grid, string searchPara)
        {
            string searchString = string.Empty;
            if (orgData != null)
            {
                orgData.RejectChanges();
                orgData.Clear();
                
                grid.ItemsSource = null;
                grid.Items.Clear();
                grid.Items.Refresh();
            }
            //if (searchPara.Contains(";"))
            //{
            //    searchString = string.Format(" WHERE {0} IN ( ", _SearchFilterColumn);
            //    string[] searchMulti = searchPara.Split(';');

            //    int total = searchMulti.Count();
            //    int lastOne = 1;
            //    foreach (var i in searchMulti)
            //    {
            //        if(total==lastOne)
            //            searchString += string.Format(" '{0}'", i);
            //        else
            //            searchString += string.Format(" '{0}',", i);
            //        lastOne++;
            //    }
            //    searchString += ")";
            //}
            //else
            //{
            //    if (searchPara.Trim() == string.Empty)
            //        searchString = string.Empty;
            //    else
            //        searchString = string.Format(" WHERE {0} {1} '{2}' ", _SearchFilterColumn, searchPara.Contains("%") ? "like" : "=", searchPara);
            //}
            //FillDataGrid(grid, string.Format("{0} {1}", _CmdString, searchString));
            searchString = _CmdString.Replace("@stregkode", string.Format("'{0}'", searchPara));
            Status = string.Format("Søger efter {0}", searchPara);
            FillDataGrid(grid, searchString);
        }

        //private void Search(string searchPara)
        //{
        //    string searchString = string.Empty;
        //    if (searchPara.Contains(";"))
        //    {
        //        searchString = " WHERE";
        //        string[] searchMulti = searchPara.Split(';');
        //        foreach (var i in searchMulti)
        //        {
        //            searchString += string.Format(" {0} AND");
        //        }
        //    }
        //    else
        //    {
        //        searchString = searchPara;
        //    }
        //    myData.FillDataGrid(grdTests, searchString);
        //}

        public void FillDataGrid(DataGrid grid, string cmdString)
        {
            Mouse.OverrideCursor = Cursors.Wait;

            try
            {
                List<int> hideMe = new List<int>();
                List<int> validateMe = new List<int>();
                List<int> readOnly = new List<int>();

                if (orgData != null) orgData.Columns.Clear();

                grid.Columns.Clear();
                grid.DataContext = null;

                if (cmdString.Trim().Length < 1)
                    cmdString = _CmdString;

                CurrentCommand = cmdString;
                using (SqlConnection con = new SqlConnection(_ConString))
                {
                    SqlCommand cmd = new SqlCommand(cmdString, con);
                    sda = new SqlDataAdapter(cmd);
                    orgData = new DataTable(_TableName);
                    if (orgData != null)
                    {
                        sda.Fill(orgData);
                        if (orgData != null)
                        {
                            foreach (DataColumn col in orgData.Columns)
                            {
                                Binding binding = new Binding(string.Format("[{0}]", col.ColumnName));

                                if (ReadOnlyFieldsList.Contains(col.ColumnName))
                                {
                                    col.ReadOnly = true;
                                    binding.Mode = BindingMode.OneWay;
                                    readOnly.Add(col.Ordinal);
                                }
                                else
                                {
                                    binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
                                    binding.Mode = BindingMode.TwoWay;
                                }

                                if (HiddenFieldsList.Contains(col.ColumnName)) hideMe.Add(col.Ordinal);

                                if (ValidationFieldsList.Contains(col.ColumnName)) validateMe.Add(col.Ordinal);

                                if (col.DataType == typeof(DateTime))
                                {
                                    SetupDataColumn(binding,col, ref grid);
                                }
                                else
                                {
                                    grid.Columns.Add(new DataGridTextColumn
                                    {
                                        Header = col.ColumnName,
                                        Binding = binding,            // new Binding(string.Format("[{0}]", col.ColumnName)),
                                        Width = DataGridLength.Auto,
                                        CanUserSort = true,
                                        SortMemberPath = col.ColumnName
                                    });
                                }
                            }
                            AddDefaultValues(ref orgData);
                            grid.DataContext = orgData;
                            HideColumns(grid, hideMe);
                            SetColorsForBackGround(grid, validateMe, ValidationBackGroundColor, ValidationForeGroundColor);
                            SetColorsForBackGround(grid, readOnly, ReadOnlyBackGroundColor, ReadOnlyForeGroundColor);
                        }
                        grid.ItemsSource = orgData.DefaultView;
                        Status = "Data indlæst";
                    }
                }
            }
            catch (Exception ex)
            {
                Status = "Der opstod en fejl da tabellen skulle indlæse data: " + ex.Message;
                myLog.Log(Status, LogLevel.Error);
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }

        public void SaveDataGrid(DataGrid grid)
        {
            Mouse.OverrideCursor = Cursors.Wait;
            try
            {
                if (orgData != null)
                {
                    myLog.Log("Saving data ", LogLevel.Info);
                    //put the modified DataSet into a new DataSet(myChangedDataset) 
                    DataTable myChangedDataset = orgData.GetChanges();
                    if (myChangedDataset != null)
                    {
                        //get how many rows changed 
                        int modifiedRows = 0;
                        using (SqlConnection con = new SqlConnection(_ConString))
                        {
                            string cmdUpdate = string.Format("UPDATE {0}  SET", _TableName);

                            foreach (var i in ValidationFieldsList)
                            {
                                cmdUpdate += string.Format(" {0} = @{0},", i);
                            }
                            cmdUpdate = cmdUpdate.Substring(0, cmdUpdate.Length - 1);
                            cmdUpdate += string.Format(" WHERE {0} = @{0}", _PrimaryKey);

                            sda = new SqlDataAdapter();
                            modifiedRows = 0;

                            foreach (DataRow x in myChangedDataset.Rows)
                            {
                                SqlCommand cmdX = new SqlCommand(cmdUpdate, con);

                                cmdX.Parameters.AddWithValue(string.Format("@{0}", _PrimaryKey), (Guid)x["ID"]);

                                foreach (var i in ValidationFieldsList)
                                {
                                    cmdX.Parameters.AddWithValue(string.Format("@{0}", i), x[i]);
                                }

                                sda.UpdateCommand = cmdX;
                                modifiedRows += this.sda.Update(myChangedDataset);
                            }
                        }
                        Status = string.Format("Database has been updated successfully: {0} Modified row(s) ", modifiedRows);
                        orgData.AcceptChanges();
                    }
                    else
                    {
                        Status = "Intet at gemme";
                    }
                }
                else
                    Status = "Intet at gemme";
            }
            catch (Exception ex)
            {
                // if something somehow went wrong 
                //Status = "An error occurred updating the database: " + ex.Message;
                Status = "Der opstod en fejl da databasen skulle opdateres: " + ex.Message;
                myLog.Log(Status, LogLevel.Error);

                MessageBox.Show("Der opstod en fejl da databasen skulle opdateres: " + ex.Message, "Error", MessageBoxButton.OK,MessageBoxImage.Error);
                if(orgData!=null)
                    orgData.RejectChanges(); //cancel the changes 
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }

        private void HideColumns(DataGrid grid, List<int> hideMe)
        {
            foreach (var i in hideMe)
            {
                grid.Columns[i].Visibility = Visibility.Collapsed;
            }
        }

        private void SetColorsForBackGround(DataGrid grid, List<int> colorMe, string newBackGroundColor, string newForeGroundColor)
        {
            try
            {
                var conv = new UniversalValueConverter();

                Style colCol = new Style();
                colCol.TargetType = typeof(DataGridCell);
                Setter setter = new Setter();
                setter.Property = DataGridCell.BackgroundProperty;
                setter.Value = conv.Convert(newBackGroundColor, DataGridCell.BackgroundProperty.PropertyType, null, CultureInfo.InvariantCulture);
                colCol.Setters.Add(setter);
                Setter set1 = new Setter();
                set1.Property = DataGridCell.ForegroundProperty;
                set1.Value = conv.Convert(newForeGroundColor, DataGridCell.ForegroundProperty.PropertyType, null, CultureInfo.InvariantCulture);
                colCol.Setters.Add(set1);

                foreach (var x in colorMe)
                {
                    grid.Columns[x].CellStyle = colCol;
                }
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        private void AddDefaultValues(ref DataTable myTable)
        {
            try
            {
                if (defaultValues.Keys.Count() > 0)
                {
                    foreach (DataColumn dc in myTable.Columns)
                    {
                        if (defaultValues.ContainsKey(dc.ColumnName))
                        {
                            foreach (DataRow d in myTable.Rows)
                            {
                                string x = d[dc.ColumnName].ToString();
                                if (string.IsNullOrEmpty(x))
                                {
                                    string newValue = string.Empty;
                                    // Use default value
                                    if (defaultValues.TryGetValue(dc.ColumnName, out newValue))
                                    {
                                        d[dc.ColumnName] = newValue;
                                        //dc.DataType
                                        d.AcceptChanges();
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                myLog.Log(string.Format("Error in AddDefaultValues: {0}", ex.Message), LogLevel.Error);
            }
        }

        public void SaveComments(string CommentToSave, string searchString)
        {
            try
            {
                myLog.Log(string.Format("Saving comment: {0}, SearchString: {1}", CommentToSave, searchString), LogLevel.Info);
                string currentUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                using (SqlConnection con = new SqlConnection(_ConString))
                {
                    string cmdUpdate = "INSERT INTO COMMENTS ( Comment, UserInit, TimeOfComment, SearchString) VALUES(@Comment, @UserInit, @TimeOfComment, @SearchString)";
                    SqlDataAdapter sda = new SqlDataAdapter();
                    SqlCommand cmdX = new SqlCommand(cmdUpdate, con);
                    cmdX.Parameters.AddWithValue("@Comment", CommentToSave);
                    cmdX.Parameters.AddWithValue("@UserInit", currentUser);
                    cmdX.Parameters.AddWithValue("@SearchString", searchString);

                    SqlParameter timeOfComment = new SqlParameter("@TimeOfComment", SqlDbType.DateTime);
                    timeOfComment.Value = DateTime.Now;
                    cmdX.Parameters.Add(timeOfComment);

                    con.Open();
                    cmdX.ExecuteNonQuery();
                    con.Close();

                    Status = string.Format("Kommentar gemt: {0}", CommentToSave);
                }

            }
            catch (Exception ex)
            {
                Status = "Fejl ved forsøg på at gemme kommentar";
                myLog.Log(string.Format("{0}: {1}", Status, ex.Message), LogLevel.Error);
            }
        }

        private void SetupDataColumn(Binding binding, DataColumn col, ref DataGrid grid)
        {
            //   col.DateTimeMode=
            binding.StringFormat = "{0:dd-MM-yyyy}";

            RegexValidationRule rule = new RegexValidationRule();
            rule.RegexText = DateFormatRegex.DateRegex;

            rule.ErrorMessage = "Forkert dato format.";
            rule.RegexOptions = RegexOptions.IgnoreCase;

            binding.ValidatesOnDataErrors = true;
            binding.ValidatesOnExceptions = true;
            binding.ValidationRules.Add(rule);

            DataGridTemplateColumn dgt = new DataGridTemplateColumn();
            dgt.Header = col.ColumnName;
            dgt.Width = DataGridLength.Auto;
            dgt.CanUserSort = true;
            dgt.SortMemberPath = col.ColumnName;

            FrameworkElementFactory fef = new FrameworkElementFactory(typeof(DatePicker));
            fef.SetValue(DatePicker.SelectedDateProperty, binding);
            fef.SetValue(DatePicker.DisplayDateProperty, binding);
            DataTemplate cellTemplate = new DataTemplate();
            cellTemplate.VisualTree = fef;
            dgt.CellTemplate = cellTemplate;
            grid.Columns.Add(dgt);
            //grid.Columns.Add(new DataGridTextColumn
            //{
            //    Header = col.ColumnName,
            //    Binding = binding,            // new Binding(string.Format("[{0}]", col.ColumnName)),
            //    Width = DataGridLength.Auto,
            //    CanUserSort = true,
            //    SortMemberPath = col.ColumnName
            //});

        }

        #region Properties

        private string _Status = "OK";

        public string Status { get { return _Status; } set { _Status = value; NotifyPropertyChanged("Status"); myLog.Log(Status, LogLevel.Debug); } }

        #endregion Properties

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        #endregion INotifyPropertyChanged


    }
}
