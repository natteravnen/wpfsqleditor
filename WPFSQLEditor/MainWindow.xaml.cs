﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace WPFSQLEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        string _ConString = string.Empty;
        string _CmdString = string.Empty;
        string _TableName = string.Empty;
        WPFViewModel myViewModel = new WPFViewModel();


        DB_Data myData;
        public MainWindow()
        {
            InitializeComponent();
            myData = new DB_Data();
            this.DataContext = myViewModel;
            myData.PropertyChanged += MyData_PropertyChanged;
        }

        private void MyData_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "Status")
            {
                myViewModel.Status = myData.Status;
            }
        }

        private void ReadSetup()
        {
            myViewModel.ValidationBackGroundColor = ConfigurationManager.AppSettings["ValidationBackGroundColor"];
            myViewModel.ValidationForeGroundColor = ConfigurationManager.AppSettings["ValidationForeGroundColor"];
            myViewModel.ReadOnlyBackGroundColor = ConfigurationManager.AppSettings["ReadOnlyBackGroundColor"];
            myViewModel.ReadOnlyForeGroundColor = ConfigurationManager.AppSettings["ReadOnlyForeGroundColor"];
            myViewModel.IntroLegend = ConfigurationManager.AppSettings["IntroLegend"];
            myViewModel.MainTitle = ConfigurationManager.AppSettings["MainTitle"];
        }

        private void SaveDataGrid()
        {
            myData.SaveDataGrid(this.grdTests);
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            SearchForFilter();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveDataGrid();
            this.txtFilter.Focus();
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propName));
        }

        #endregion INotifyPropertyChanged

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            ReadSetup();
            this.Background = new ImageBrush(new BitmapImage(new Uri(@"pack://application:,,,/WPFSQLEditor;component/Pictures/MainBackground.jpg")));
            OnStartup();
        }

        #region TextBoxSelectionHandler

        //protected override void OnStartup(StartupEventArgs e)
        private void OnStartup()
        {
            // Select the text in a TextBox when it receives focus.
            EventManager.RegisterClassHandler(typeof(TextBox), TextBox.PreviewMouseLeftButtonDownEvent,
                new MouseButtonEventHandler(SelectivelyIgnoreMouseButton));
            EventManager.RegisterClassHandler(typeof(TextBox), TextBox.GotKeyboardFocusEvent,
                new RoutedEventHandler(SelectAllText));
            EventManager.RegisterClassHandler(typeof(TextBox), TextBox.MouseDoubleClickEvent,
                new RoutedEventHandler(SelectAllText));
            //base.OnStartup(e);
        }

        void SelectivelyIgnoreMouseButton(object sender, MouseButtonEventArgs e)
        {
            // Find the TextBox
            DependencyObject parent = e.OriginalSource as UIElement;
            while (parent != null && !(parent is TextBox))
                parent = VisualTreeHelper.GetParent(parent);

            if (parent != null)
            {
                var textBox = (TextBox)parent;
                if (!textBox.IsKeyboardFocusWithin)
                {
                    // If the text box is not yet focused, give it the focus and
                    // stop further processing of this click event.
                    textBox.Focus();
                    e.Handled = true;
                }
            }
        }

        void SelectAllText(object sender, RoutedEventArgs e)
        {
            var textBox = e.OriginalSource as TextBox;
            if (textBox != null)
                textBox.SelectAll();
        }

        #endregion TextBoxSelectionHandler

        private void txtFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SearchForFilter();
                txtFilter.SelectionStart = 0;
                txtFilter.SelectionLength = txtFilter.Text.Length;
            }
        }

        private void SearchForFilter()
        {
            string searchForMe = txtFilter.Text.Trim();
            this.txtComments.Text = string.Empty;
            myData.Search(grdTests, searchForMe);
        }

        private void txtFilter_GotFocus(object sender, RoutedEventArgs e)
        {
        }

        private void btnSaveComments_Click(object sender, RoutedEventArgs e)
        {
            myData.SaveComments(this.txtComments.Text, this.txtFilter.Text);
        }

        private void btnBadCard_Click(object sender, RoutedEventArgs e)
        {
            myData.SaveComments("Card not found", this.txtFilter.Text);
        }

        private void btnBadCPR_Click(object sender, RoutedEventArgs e)
        {
            myData.SaveComments("Bad CPR no.", this.txtFilter.Text);
        }
    }
}
